let () =
  Scanf.scanf "%d %d %d" (fun w a b ->
      let fr = min a b and re = max a b in 
      max (re - fr - w) 0)
  |> Printf.printf "%d\n"