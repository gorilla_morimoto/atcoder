# https://beta.atcoder.jp/contests/abc054/submissions/2139048

N,M=gets.split.map &:to_i
G=Array.new(N){Array.new(N)}
M.times do
  a,b=gets.split.map &:to_i
  a-=1
  b-=1
  G[a][b] = G[b][a] = true
end
ans=0
(1...N).to_a.permutation do |p|
  ok=true
  ([0]+p).each_cons(2) do |u,v|
    ok=false unless G[u][v]
  end
  ans+=1 if ok
end
p ans
