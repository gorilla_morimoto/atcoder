n, m = gets.split.map(&:to_i)
a = []
b = []

n.times { a << gets.chomp }
m.times { b << gets.chomp }

all_read = false

0.upto(n - m) do |row_a| # 入力Sの何行目を見ているか
  0.upto(n - m) do |i| # 行の何文字目か
    0.upto(m - 1) do |j| # テンプレの何行目を見ているか
      # b[j] != a[row_a][i, m]で抜けたたのか、
      # すべて見終わって抜けたのか
      all_read = false
      break unless b[j] == a[row_a][i, m]
      all_read = true
      row_a += 1
    end
    next unless all_read
    puts 'Yes'
    exit
  end
end

puts 'No'
