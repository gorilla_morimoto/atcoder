let () =
  let x = read_int () in
  (x / 11 * 2 + match x mod 11 with 0 -> 0 | y when y <= 6 -> 1 | _ -> 2)
  |> Printf.printf "%d\n"

