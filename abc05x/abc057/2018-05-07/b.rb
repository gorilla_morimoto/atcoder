n, m = gets.split.map(&:to_i)
ab = []
cd = []

n.times { ab << gets.split.map(&:to_i) }
m.times { cd << gets.split.map(&:to_i) }

ab.each do |ab_|
  tmp = 1 << 31
  ans = 0
  cd.each_with_index do |cd_, i|
    now = (ab_[0] - cd_[0]).abs + (ab_[1] - cd_[1]).abs
    if now < tmp
      ans = i + 1
      tmp = now
    end
  end
  p ans
end
