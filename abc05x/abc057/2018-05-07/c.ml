let digits n =
  let rec aux m ans =
    if m < 1 then ans else aux (m / 10) (succ ans)
  in
  aux n 0

let solve n =
  let b = sqrt n |> int_of_float in
  let intn = int_of_float n in
  let rec aux b' =
    if intn mod b' = 0 then max (digits b') (digits (intn / b'))
    else aux (pred b')
  in
  aux b

let () = read_float() |> solve |> Printf.printf "%d\n"
