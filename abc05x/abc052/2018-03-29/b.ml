let n = read_int ()

let s = read_line ()

let rec f ans now str =
  let len = String.length str in
  if len = 0 then ans
  else if str.[0] = 'I' then
    f (max ans (now + 1)) (now + 1) (String.sub str 1 (len - 1))
  else f ans (now - 1) (String.sub str 1 (len - 1))


let () = f 0 0 s |> Printf.printf "%d\n"