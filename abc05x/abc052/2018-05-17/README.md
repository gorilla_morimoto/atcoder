# ABC052

2018-05-17

## テストケース

https://www.dropbox.com/sh/arnpe0ef5wds8cv/AAB4T7orLnIlhXdg4g5xC2dUa/ARC067?dl=0

## A - Two Rectangles

- 「やるだけ案件」

## B - Increment Decrement

- Nの最大値は100なのでシミュレーションしてしまうのが一番簡単そう

- というかシミュレーションじゃないと完全に正確な値は求められないのでは……？

- `Array.init`を使うか、`String.iter`を使うかの2パターン考えられる
  - 今回は`String.iter`でやります

## C - Factors of Factorial

- Nの最大値が`10^3`なので階乗はまともに計算できない

- Nから1までの数字のすべての組み合わせ？
  - N=6
    - 1, 2, 3, 4, 5, 6
      - 2: 2, 6, 8, 10, 12, 15, 18, 10, 24, 30
      - 3: 6,.....

- すべての組み合わせなんて計算できない

- 諦めます
