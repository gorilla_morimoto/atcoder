let () =
  Scanf.scanf "%d %d" (fun n m ->
      if m >= 2 * n then n + (m - 2 * n) / 4 else m / 2 )
  |> Printf.printf "%d\n"

