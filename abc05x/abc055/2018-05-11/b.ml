let f n =
  let m = 1000000007 in
  let rec aux i tmp =
    if i >= n then tmp
    else aux (succ i) ((tmp * (i + 1)) mod m)
  in
  aux 1 1

let () =
  f @@ read_int () |> Printf.printf "%d\n"
