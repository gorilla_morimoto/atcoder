(*https://beta.atcoder.jp/contests/abc051/submissions/1317741*)
(*この方のコードはめちゃめちゃ速いんだけど、何をやっているのか理解できない*)
open Printf
open Scanf

let () =
  let k, s = scanf "%d %d\n" (fun x y -> (x, y)) in
  let mx = min k s in
  let count t = min (max 0 (2*k-t+1)) (t+1) in
  let rec iter x a =
    if x > mx then a
    else
      iter (x+1) (a + count (s-x)) in
  iter 0 0 |> printf "%d\n"
