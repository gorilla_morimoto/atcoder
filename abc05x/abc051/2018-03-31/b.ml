let k, s = Scanf.scanf "%d %d" (fun k s -> k, s)
let ans = ref 0

let () =
  for x = 0 to k do 
    for y = 0 to k do 
      if (s - x - y >= 0 && s - x - y <= k) then incr ans
    done ;
  done ;
  Printf.printf "%d\n" !ans