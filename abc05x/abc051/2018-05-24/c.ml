let rep n s =
  let rec aux n =
    if n > 0 then (print_string s; aux (pred n))
  in
  aux n

let () =
  Scanf.scanf "%d %d %d %d" (fun sx sy tx ty ->
      let x = abs (tx - sx) and y = abs (ty - sy) in
      rep x "R";
      rep y "U";
      rep x "L";
      rep y "D";
      print_string "D";
      rep (succ x) "R";
      rep (succ y) "U";
      print_string "L";
      print_string "U";
      rep (succ x) "L";
      rep (succ y) "D";
      print_string "R";
      print_newline ();
    )