let () = 
  Scanf.scanf "%d %d %d" (fun a b c ->
      if (b - a = c - b) then "YES" else "NO")
  |> print_endline
