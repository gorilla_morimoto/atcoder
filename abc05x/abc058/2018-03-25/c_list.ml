let n = Scanf.scanf "%d " (fun x -> x)

let ls = Array.init n (fun _ -> Scanf.scanf "%s " (fun s -> s)) |> Array.to_list

let buf = Buffer.create 50

let remove_char s c =
  Str.replace_first (Str.regexp_string (Char.escaped c)) "" s


let string_to_char_list s =
  let rec f i ls =
    if i = String.length s then ls else f (i + 1) (s.[i] :: ls)
  in
  f 0 []
