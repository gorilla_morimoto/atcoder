let o = read_line ()
let e = read_line ()
let buf = Buffer.create (String.length o * 2)

let () =
  if String.length o = String.length e then
    for i = 0 to (String.length o - 1) do
      Buffer.add_char buf o.[i] ;
      Buffer.add_char buf e.[i] ;
    done
  else
    begin
      for i = 0 to (String.length e - 1) do
        Buffer.add_char buf o.[i] ;
        Buffer.add_char buf e.[i] ;
      done ;
      Buffer.add_char buf o.[String.length o - 1]
    end ;
  print_endline (Buffer.contents buf)