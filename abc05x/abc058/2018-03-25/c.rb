n = gets.to_i
ss = $stdin.read.split.sort { |before, after| before.length - after.length }

ans = ''
shortest = ss.shift
shortest.chars do |c|
  next unless ss.all? { |s| s.include? c }
  ans << c
  ss.each do |s|
    s.slice! c # deleteだと複数の文字が消えてしまう
  end
end

puts ans.split('').sort.join('')