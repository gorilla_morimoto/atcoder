def calc(n)
  (2**n) % 1000000007
end

n = gets.to_i
a = Array.new(n, 0)
gets.split.each do |x|
  a[x.to_i] += 1
end

if n.odd?
  a[0] += 1
  0.step(n - 1, 2) do |i|
    next if a[i] == 2
    puts 0
    exit
  end
else
  1.step(n - 1, 2) do |i|
    next if a[i] == 2
    puts 0
    exit
  end
end

puts calc(n / 2)
