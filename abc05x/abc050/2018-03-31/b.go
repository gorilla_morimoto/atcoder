package main

import (
	"bytes"
	"fmt"
)

var (
	n    int
	m    int
	sum  int
	tmp  int
	p, x int
)

func main() {
	b := bytes.NewBufferString("")

	fmt.Scan(&n)
	t := make([]int, n)
	for i := 0; i < n; i++ {
		fmt.Scan(&tmp)
		t[i] = tmp
		sum += tmp
	}

	fmt.Scan(&m)
	for i := 0; i < m; i++ {
		fmt.Scan(&p, &x)
		b.WriteString(fmt.Sprintln(sum - (t[p-1] - x)))
	}

	fmt.Print(b.String())

}
