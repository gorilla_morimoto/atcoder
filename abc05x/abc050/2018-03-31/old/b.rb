# なぜかリジェクトされた
# require 'stringio'のせいみたい
require 'stringio'

gets
t = gets.split.map(&:to_i)
sum = t.inject(:+)
m = gets.to_i
sio = StringIO.new('', 'r+')

m.times do
  line = gets.split.map(&:to_i)
  sio.write(sum - (t[line[0] - 1] - line[1]), "\n")
end

print sio.string
