# ABC050

2018-05-26

## テストケース

https://www.dropbox.com/sh/arnpe0ef5wds8cv/AAD8HDknbvINk8MmRU7UDIzaa/ARC066?dl=0

## A - Addition and Subtraction Easy

- やるだけ案件

## B - Contest with Drinks Easy

- Tを配列で受け取る
- Tの合計を計算する
- `p-1`をインデックスとしてそこの値とxの差をTの和から引く

## C - Lining Up

- ソートして頭から見ていく
- Nが奇数の場合
  - 0がひとつ、偶数が2個ずつ`N-1`まで存在するか確認
- Nが偶数の場合
  - 奇数が2個ずつ1から`N-1`まで存在するか確認

