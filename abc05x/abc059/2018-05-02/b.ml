let gr = "GREATER"
let le = "LESS"
let eq = "EQUAL"

let f a b =
  let len = String.length a in
  let rec g i =
    if i >= len then eq
    else
      let ca = Char.code a.[i] and cb = Char.code b.[i] in
      if ca > cb then gr else if cb > ca then le
      else g (succ i)
  in
  g 0

let () =
  let a = read_line () and b = read_line () in
  let lena = String.length a and lenb = String.length b in
  (if lena > lenb then gr else if lenb > lena then le
   else f a b)
  |> print_endline

