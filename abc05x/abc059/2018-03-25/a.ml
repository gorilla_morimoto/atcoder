let up c = String.uppercase (Char.escaped c)
let () =
  Scanf.scanf "%s %s %s" (fun s1 s2 s3 -> 
      (up s1.[0]) ^ (up s2.[0]) ^ (up s3.[0]))
  |> print_endline