# ABC059

2018-03-25 3:48

## テストケース

https://www.dropbox.com/sh/arnpe0ef5wds8cv/AAAfT6_AJRiIzvPmt2_1fLYYa/ARC072?dl=0

## A - Three-letter acronym

- `s1.[0]`としてしまうと得られるのは`char`になってしまう

- `String.uppercase`は`deprecated`ですが、代替の`String.uppercase_ascii`は`4.03`かららしいので使えません

4:01

## B - Comparison

- 単純に比較して終了、とはならない模様

- 大きすぎる数は`Exception: Failure "int_of_string".`が発生してしまう

- 文字列として扱って長さを測る？

- 文字列が同じだった場合の処理が漏れてしまってTLEが出た

4:21

## C - Sequence

- 配列の頭から足していく
- 正負が変わらなかったら`count`に調整に必要な数だけ追加
- `count`を出力

- 5:11
  - 提出しましたが、WAがけっこう多いです
  - かなり考えた末での提出なのでもう解説を読みます

- 6:48
  - もう無理です
  - ギブアップ
  - ヘタに関数型言語を使っているからか解説を読んでも実装できません
  - Rubyで書こうとしてもわけわからなくなります
  - この回はじめてからもう3時間ですよ
    - 信じられない
    - 意外と集中力ありますね
    - 時間をメモしておいてよかったですね
  