(*解説の実装*)
let ar = [|0;1;3;1;2;1;2;1;1;2;1;2;1|]
let () =
  Scanf.scanf "%d %d" (fun x y -> 
      (if ar.(x) = ar.(y) then "Yes" else "No")
      |> print_endline)