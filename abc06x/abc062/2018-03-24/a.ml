let () =
  let a = [1; 3; 5; 7; 8; 10; 12] and b = [4; 6; 9; 11] in
  Scanf.scanf "%d %d" (fun x y ->
      if List.exists (( = ) x) a && List.exists (( = ) y) a
      || List.exists (( = ) x) b && List.exists (( = ) y) b
      || x = 2 && y = 2
      then "Yes"
      else "No" )
  |> print_endline

