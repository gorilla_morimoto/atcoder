let h, w = Scanf.scanf "%d %d\n" (fun h w -> (h, w))

let repeat i =
  let rec aux tmp n = if n = 0 then tmp else aux ("#" ^ tmp) (n - 1) in
  aux "" i

let line = repeat (w + 2)

let buf = Buffer.create (h * w)

let rec f n =
  if n = h then ()
  else
    let () =
      Buffer.add_string buf (Printf.sprintf "#%s#\n" (Scanf.scanf "%s\n" (fun s -> s)))
    in
    f (n + 1)


let () =
  Buffer.add_string buf (Printf.sprintf "%s\n" line) ;
  f 0 ;
  Buffer.add_string buf (Printf.sprintf "%s\n" line) ;
  Printf.printf "%s" (Buffer.contents buf)

