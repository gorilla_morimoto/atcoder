let max3 a b c = max (max a b) c
let min3 a b c = min (min a b) c

let f = function
  | a, b when a mod 3 = 0 || b mod 3 = 0 -> 0
  | a, b when a mod 2 = 1 ->
    let upper = (a / 2) * b in
    let low_l = b / 2 in
    let low_r = b - low_l in
    let lower_l = (a / 2 + 1) * low_l in
    let lower_r = (a / 2 + 1) * low_r in
    (max3 upper lower_l lower_r) - (min3 upper lower_l lower_r)
  | 2, b ->
    let ott = b / 3 in
    let left = ott * 2 in
    let right = (b - 2 * ott) * 2 in
    abs (left - right)
  | a, b ->
    let upper = (a / 2 - 1) * b in
    let low_l = b / 2 in
    let low_r = b - low_l in
    let lower_l = (a / 2 + 1) * low_l in
    let lower_r = (a / 2 + 1) * low_r in
    (max3 upper lower_l lower_r) - (min3 upper lower_l lower_r)

let () =
  Scanf.scanf "%d %d" (fun a b -> min (f (a,b)) (f (b,a)))
  |> Printf.printf "%d\n"