let ar = [|1;3;1;2;1;2;1;1;2;1;2;1|]
let f x y = if ar.(x-1) = ar.(y-1) then "Yes" else "No"
let () = Scanf.scanf "%d %d" f |> print_endline