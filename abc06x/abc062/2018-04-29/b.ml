let f h w =
  let rec pr_hash w' =
    if w' < -1 then print_newline ()
    else (print_char '#' ; pr_hash (pred w'))
  in
  let rec pr_row h' =
    if h' <= 0 then ()
    else
      Scanf.scanf "%s\n" (fun row ->
          Printf.printf "#%s#\n" row; pr_row (pred h'))
  in
  pr_hash w;
  pr_row h;
  pr_hash w;;

let () = Scanf.scanf "%d %d\n" f