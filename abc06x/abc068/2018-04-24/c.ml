open Hashtbl

let f a b = (a, b)
let n, m = Scanf.scanf "%d %d\n" f
let h = create n

let rec inith x =
  if x = 0 then ()
  else
    match Scanf.scanf "%d %d\n" f with
    | 1, a ->
      add h 1 a ;
      inith (pred x)
    | a, b when b = n ->
      add h a n ;
      inith (pred x)
    | _ -> inith (pred x)


let rec solve = function
  | [] -> "IMPOSSIBLE"
  | hd :: tl -> if mem h hd then "POSSIBLE" else solve tl


let () =
  inith m ;
  solve (find_all h 1) |> print_endline

