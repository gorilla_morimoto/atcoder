module Int = struct
  type t = int
  let compare = compare
end

module Ints = Set.Make(Int)