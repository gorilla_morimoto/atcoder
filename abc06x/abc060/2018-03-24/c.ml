let n, t = Scanf.scanf "%d %d\n" (fun n t -> (n, t))

let ar = Array.init n (fun _ -> Scanf.scanf "%d " (fun x -> x))

let rec f i sum =
  if i = n then sum + t
  else if ar.(i) - ar.(i - 1) <= t then f (i + 1) (sum + ar.(i) - ar.(i - 1))
  else f (i + 1) (sum + t)


let () = f 1 0 |> Printf.printf "%d\n"
