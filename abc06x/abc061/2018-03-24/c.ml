let scan_words () = Scanf.scanf "%d %d\n" (fun x y -> (x, y))

let () =
  let n, k = scan_words () in
  let ar = Array.make 100001 0 in
  for i = 1 to n do
    let a, b = scan_words () in
    ar.(a) <- ar.(a) + b
  done ;
  let rec f i tmp =
    let new_tmp = tmp - ar.(i) in
    if new_tmp <= 0 then i else f (i + 1) new_tmp
  in
  f 0 k |> Printf.printf "%d\n"

