package main

import "fmt"

var (
	x, a, b int
)

func main() {
	fmt.Scan(&x, &a, &b)

	if b-a <= 0 {
		fmt.Println("delicious")
	} else if (b-a)-x <= 0 {
		fmt.Println("safe")
	} else {
		fmt.Println("dangerous")
	}
}
