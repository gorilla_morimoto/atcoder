let () =
  Scanf.scanf "%d %d %d" (fun x a b ->
      if b - a <= 0 then "delicious"
      else if b - a - x <= 0 then "safe"
      else "dangerous" )
  |> Printf.printf "%s\n"

