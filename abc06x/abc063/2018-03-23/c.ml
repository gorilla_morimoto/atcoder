let n = read_int ()

let ss =
  let rec read i ls = 
    if i = n then ls else read (i + 1) (read_int () :: ls)
  in
  read 0 []


let rec find_min tmp = function
  | [] -> (if tmp = max_int then 0 else tmp)
  | h :: tl ->
    if h mod 10 = 0 then find_min tmp tl else find_min (min tmp h) tl


let () =
  let sum = List.fold_left ( + ) 0 ss in
  ( if sum mod 10 <> 0 then sum
    else
      let new_sum = sum - find_min max_int ss in
      if new_sum mod 10 <> 0 then new_sum else 0 )
  |> Printf.printf "%d\n"

