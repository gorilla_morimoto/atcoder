package main

import (
	"bufio"
	"fmt"
	"os"
	"strconv"
)

var (
	n   int
	sum int
	tmp int
)

func main() {
	sc := bufio.NewScanner(os.Stdin)
	sc.Scan()
	n = intOfString(sc.Text())
	ss := make([]int, 0, n)

	for sc.Scan() {
		tmp = intOfString(sc.Text())
		sum += tmp
		ss = append(ss, tmp)
	}

	if sum%10 == 0 {
		min := findMin(ss)
		if min == 101 {
			sum = 0
		} else {
			sum -= min
		}
	}

	fmt.Println(sum)
}

func intOfString(s string) int {
	x, _ := strconv.Atoi(s)
	return x
}

func findMin(ls []int) int {
	tmp := 101
	for _, v := range ls {
		if v%10 != 0 && v < tmp {
			tmp = v
		}
	}

	return tmp
}
