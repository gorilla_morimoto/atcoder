module Is = Set.Make (struct type t = int let compare = compare end)
let id x = x

let solve n =
  let rec aux n' s o =
    if n' <= 0 then (s, o)
    else
      match Scanf.scanf "%d " id / 400 with
      | a when a < 8 -> aux (pred n') (Is.add a s) o
      | _ -> aux (pred n') s (succ o)
  in
  aux n Is.empty 0

let () =
  Scanf.scanf "%d\n" solve
  |> (fun (s, o) -> (max 1 (Is.cardinal s), Is.cardinal s + o))
  |> fun (mi, ma) -> Printf.printf "%d %d\n" mi ma

