n = gets.to_i
a = gets.split.map(&:to_i)
flag = Array.new(8) { false }
over = 0
count = 0

a.each do |x|
  if x >= 3200
    over += 1
    next
  end

  flag[x / 400] = true
end

flag.each do |f|
  count += 1 if f
end

printf("%d %d\n", [1, count].max, count + over)
