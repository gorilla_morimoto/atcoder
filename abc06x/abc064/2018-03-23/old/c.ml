(*sample/c_sly.goを参考に実装*)
let () =
  let n = read_int () in
  let flag = Array.make 8 false in
  let other = ref 0 in
  let rec peek i =
    if i = 0 then ()
    else
      let x = Scanf.scanf "%d " (fun x -> x) in
      if x >= 3200 then other := !other + 1 else flag.(x / 400) <- true ;
      peek (i - 1)
  in
  let minimum = ref 0 in
  let rec f i' =
    if i' = 8 then ()
    else if flag.(i') then
      let () = minimum := !minimum + 1 in
      f (i' + 1)
    else f (i' + 1)
  in
  let () = peek n ; f 0 in
  Printf.printf "%d %d\n" (max 1 !minimum) (!minimum + !other)

