let solve n k =
  let rec mkls n' tmp =
    if n' <= 0 then List.fast_sort (fun i j -> j - i) tmp
    else
      mkls (pred n') ((Scanf.scanf "%d " (fun x -> x)) :: tmp)
  in
  let rec sum i ans ls =
    if i > k then ans
    else sum (succ i) (ans + List.hd ls) (List.tl ls)
  in
  mkls n [] |> sum 1 0

let () = Scanf.scanf "%d %d\n" solve |> Printf.printf "%d\n"