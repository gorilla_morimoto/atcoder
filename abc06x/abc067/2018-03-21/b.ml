let n, k = Scanf.scanf "%d %d\n" (fun x y -> (x, y))

let a = Array.init n (fun _ -> Scanf.scanf "%d " (fun x -> x))

let () =
  Array.fast_sort (fun a b -> b - a) a ;
  Array.sub a 0 k |> Array.fold_left (+) 0
  |> Printf.printf "%d\n"

