let n = Scanf.scanf "%d\n" (fun x -> x)

let a = Array.init n (fun _ -> Scanf.scanf "%d " (fun x -> x))

let total = Array.fold_left ( + ) 0 a

let () =
  let rec f i sumx tmp =
    if i = n then tmp
    else
      let next_sumx = sumx + a.(i) in
      let next_tmp = min tmp (abs (2 * sumx - total)) in
      f (i + 1) next_sumx next_tmp
  in
  f 1 a.(0) max_int |> Printf.printf "%d\n"

