package main

import "fmt"

var (
	a, b, c int
)

func main() {
	fmt.Scan(&a, &b, &c)
	fmt.Println(a + b + c - max(a, b, c))
}

func max(x, y, z int) int {
	if x < y {
		if y < z {
			return z
		}
		return y
	}

	if x < z {
		return z
	}
	return x
}
