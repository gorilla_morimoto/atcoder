def yes?(s)
  return false if s.length.odd?

  half = s.length / 2
  s[0, half] == s[half, half]
end

s = gets.chomp
(s.length - 1).downto(1) do |i|
  if yes?(s[0, i])
    puts s[0, i].length
    exit
  end
end
