let () =
  Scanf.scanf "%d %d %d" (fun a b c ->
      let sum = a + b + c in
      List.fold_left (fun acc n -> min acc (sum - n)) sum [a; b; c] )
  |> Printf.printf "%d\n"

