let id x = x
let n = Scanf.scanf "%d\n" id
let ar = Array.init n (fun _ -> Scanf.scanf "%s " id)
let addbuf i b = Buffer.add_string b (ar.(i) ^ " ")

let f () =
  let b = Buffer.create n in
  let rec g i forward =
    if forward then
      if i >= n then Buffer.contents b
      else
        let () = addbuf i b in
        g (i + 2) true
    else
      match i with
      | i when i < 0 ->
        let () = addbuf 0 b in
        g 2 true
      | 0 ->
        let () = addbuf 0 b in
        g 1 true
      | _ ->
        let () = addbuf i b in
        g (i - 2) false
  in
  g (pred n) false


let () =
  (match n with 2 -> ar.(1) ^ ar.(0) | 1 -> ar.(0) | _ -> f ()) |> String.trim
  |> print_endline

