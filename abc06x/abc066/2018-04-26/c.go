package main

import (
	"bufio"
	"fmt"
	"os"
	"strconv"
)

func main() {
	sc := bufio.NewScanner(os.Stdin)
	sc.Split(bufio.ScanWords)

	sc.Scan()
	n, _ := strconv.Atoi(sc.Text())
	i := 1
	tmp := ""
	for sc.Scan() {
		s := sc.Text()
		if i%2 == n%2 {
			tmp = s + " " + tmp //fmt.Sprintf("%s %s", s, tmp)
		} else {
			tmp = tmp + " " + s //fmt.Sprintf("%s %s", tmp, s)
		}
	}
	fmt.Println(tmp)
}
