let f s =
  let len = String.length s in
  Printf.sprintf "%c%d%c" s.[0] (len - 2) s.[len-1]

let () = f (read_line ()) |> print_endline