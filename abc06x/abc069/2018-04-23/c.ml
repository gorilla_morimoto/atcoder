let count n =
  let rec aux n' two four odd =
    if n' = 0 then (two, four, odd)
    else
      match Scanf.scanf "%d " (fun x -> x) with
      | f when f mod 4 = 0 -> aux (pred n') two (succ four) odd
      | t when t mod 2 = 0 -> aux (pred n') (succ two) four odd
      | _ -> aux (pred n') two four (succ odd)
  in
  aux n 0 0 0


let () =
  ( match Scanf.scanf "%d\n" count with
  | 0, f, o -> if f >= o - 1 then "Yes" else "No"
  | _, f, o -> if f >= o then "Yes" else "No" )
  |> print_endline

