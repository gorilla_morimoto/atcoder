(*こっちは66ms, 6912KB*)
open Hashtbl

let id x = x
let n = Scanf.scanf "%d\n" id
let h = create 100

let f n =
  let rec g n' =
    if n' = 0 then ()
    else
      let a = Scanf.scanf "%d " id in
      let v = try find h a with _ -> 0 in
      replace h a (v + 1) ;
      g (n' - 1)
  in
  g n


let () =
  f n ;
  fold (fun k v tmp -> 
      let v' = try find h (k - 1) with _ -> 0 in
      let v'' = try find h (k + 1) with _ -> 0 in
      max tmp (v + v' + v'')) h 0
  |> Printf.printf "%d\n"

