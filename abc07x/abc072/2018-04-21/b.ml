let () =
  let s = read_line () in
  let len = String.length s in
  let b = Buffer.create len in
  let cs = Array.init len (fun i -> s.[i]) in
  Array.iteri
    (fun i c -> if (i + 1) mod 2 = 1 then Buffer.add_char b c else ())
    cs ;
  print_endline (Buffer.contents b)

