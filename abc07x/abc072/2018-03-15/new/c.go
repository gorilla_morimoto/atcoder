package main

import (
	"bufio"
	"fmt"
	"os"
	"strconv"
)

var (
	m = make(map[int]int)
	n int
)

func main() {
	sc := bufio.NewScanner(os.Stdin)
	sc.Split(bufio.ScanWords)
	sc.Scan()

	x := 0
	for sc.Scan() {
		x, _ = strconv.Atoi(sc.Text())
		m[x]++
	}

	max := 0
	tmp := 0
	for k := range m {
		tmp = m[k-1] + m[k] + m[k+1]
		if max < tmp {
			max = tmp
		}
	}
	fmt.Println(max)
}
