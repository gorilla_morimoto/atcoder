package main

import (
	"fmt"
)

var (
	m = make(map[int]int)
	n int
)

func main() {
	fmt.Scan(&n)

	a := make([]int, n)
	for i := 0; i < n; i++ {
		fmt.Scan(&a[i])
	}

	for _, v := range a {
		m[v-1]++
		m[v]++
		m[v+1]++
	}

	max := 0
	for _, v := range m {
		if max < v {
			max = v
		}
	}
	fmt.Println(max)
}
