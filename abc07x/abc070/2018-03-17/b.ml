let a, b, c, d = Scanf.scanf "%d %d %d %d" (fun a b c d -> (a, b, c, d))

let () =
  let x = max a c in
  let y = min b d in
  if y - x > 0 then Printf.printf "%d\n" (y - x) else Printf.printf "%d\n" 0

