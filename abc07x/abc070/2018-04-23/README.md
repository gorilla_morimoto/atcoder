# ABC070

2018-04-23

## テストケース

https://www.dropbox.com/sh/arnpe0ef5wds8cv/AAAp6lGPg7E0TFjoZnP_CbnHa/ABC070?dl=0

## A - Palindromic Number

- 1文字目と3文字目が同じかどうか

## B - Two Switches

- (min b d) - (max a c)
- 同時に押している間がない場合は0なのでmax 0をしかける

## C - Multiple Clocks

- 最小公倍数を求めるだけっぽい
- N個の数があるので、リストにしてfold_leftでひとつずつ見ていけばよさそう

## 振り返り

### C

- 最小公倍数を求めるところで、ネットで検索すると`a * b / gcd(a, b)`というふうに出てきますが、その通りに実装してしまうと入力例2でオーバーフローしてしまいます
  - なので、`a / gcd(a, b) * b`のように、掛け算はあとにしてあげるとよいです
  - 前にやったときもここでつまづいた気がします
  - max_intはだいたい10^9っぽいです