let s = Scanf.scanf "%s" (fun x -> x);;
let a = Array.make 26 false;;

let rec split_to_cl s =
  let len = String.length s in 
  if len = 0 then []
  else
    s.[0] :: split_to_cl (String.sub s 1 (len - 1))
;;

let x = split_to_cl s
let p = Printf.printf "%c\n"

let () =
  let codes = List.map (Char.code) x in 
  let codes' = List.map ((-) 96) codes in 
  let codes'' = List.iter (fun i -> a.(i) <- true) codes' in
  List.iter ()

(*
ここで力尽きました
あとはaを頭から見ていって、falseが出てきたところで、そのインデックスが示す文字を出力する、
あるいはNoneを出力する、というだけなのですが、もう疲れました

やっぱりまだまだOCamlは慣れません
*)