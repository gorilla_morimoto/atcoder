let x, a, b = Scanf.scanf "%d %d %d" (fun x a b -> x, a, b)

let () =
  if abs (x - a) < abs (x - b)
  then print_endline "A"
  else print_endline "B"