package main

import "fmt"

var (
	x, a, b int
)

func main() {
	fmt.Scan(&x, &a, &b)

	if abs(x-a) < abs(x-b) {
		fmt.Println("A")
	} else {
		fmt.Println("B")
	}
}

func abs(x int) int {
	if x < 0 {
		return -x
	}
	return x
}
