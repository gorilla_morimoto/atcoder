let scan_word = Scanf.scanf "%d " (fun x -> x)

let () =
  let n = scan_word in
  let a = Array.init n (fun _ -> scan_word) in
  Array.fast_sort (fun x y -> compare y x) a ;
  let rec search i pre =
    if i = n then 0
    else if a.(i - 1) = a.(i) then
      if pre <> -1 then pre * a.(i) else search (i + 2) a.(i)
    else search (i + 1) pre
  in
  search 1 (-1) |> Printf.printf "%d\n"

