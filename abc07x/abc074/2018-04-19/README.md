# ABC074

2018-04-19

## テストケース

https://www.dropbox.com/sh/arnpe0ef5wds8cv/AADsRQEAEwl1dgnVtSsLDeHza/ARC083?dl=0

## A - Bichrome Cells

- n^2 - a

## B - Collecting Balls (Easy Version)

- min 2xi 2(k-xi)の和

## C - Sugar Water

- 前回もわからなかった覚えがありますが、今回もできませんでした

- 前回から何も成長していないようです……

- というのは悔しいので、一応考え方までは理解しました
  - 条件に合う水の量、条件に合う砂糖の量を列挙する
  - それぞれの組み合わせを試して最大の濃度になるものを探す
  - という感じです

- これをもとにOCamlで実装しましたが、なぜか9番目のテストケースで1だけ答えがずれます
  - 正: 912 112
  - 誤: 911 111
  - 原因がわかりませんが、疲れたのでこれまでとします