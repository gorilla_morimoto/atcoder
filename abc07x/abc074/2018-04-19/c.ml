let a, b, c, d, e, f =
  Scanf.scanf "%d %d %d %d %d %d\n" (fun a b c d e f -> (a, b, c, d, e, f))

let fw i j = (a * i + b * j) * 100
let pw x = x < f && x > 0
let fs i j = c * i + d * j
let ps x = x < f

let mk_ls f p =
  let rec inner i j ls =
    if j > 50 then ls
    else
      match f i j with
      | x when p x -> inner i (j + 1) (x :: ls)
      | _ -> inner i (j + 1) ls
  in
  let rec outer i ls =
    if i > 50 then ls
    else
      let ls' = inner i 0 ls in
      outer (i + 1) ls'
  in
  outer 1 []


let ws = mk_ls fw pw |> List.sort_uniq compare
let ss = mk_ls fs ps |> List.sort_uniq compare

let rec solve wls sls =
  let rec inner w conc suma sums = function
    | s :: t when w + s <= f ->
      let tmp = float s /. float (w + s) in
      if tmp > conc && float s <= float e /. 100. *. float w then
        inner w tmp (s + w) s t
      else inner w conc suma sums t
    | [] -> (conc, suma, sums)
    | _ :: t -> inner w conc suma sums t
  in
  let rec outer conc suma sums = function
    | w :: t ->
      let conc', suma', sums' = inner w conc suma sums sls in
      if conc < conc' then outer conc' suma' sums' t
      else outer conc suma sums t
    | [] -> (suma, sums)
  in
  outer 0. 0 0 wls


let () =
  let suma, sums = 
    match solve ws ss with
    | 0, sums -> a * 100, sums
    | suma, sums -> suma, sums
  in
  Printf.printf "%d %d\n" suma sums