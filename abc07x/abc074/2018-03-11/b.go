package main

import "fmt"

var (
	n, k int
)

func main() {
	fmt.Scan(&n, &k)

	x := make([]int, n)
	for i := 0; i < n; i++ {
		fmt.Scan(&x[i])
	}

	f(x)
	fmt.Println(sum(x))

}

func f(x []int) {
	for i, v := range x {
		if v <= abs(k-v) {
			x[i] = 2 * v
		} else {
			x[i] = abs(k-v) * 2
		}
	}
}

func abs(x int) int {
	if x < 0 {
		return -x
	}

	return x
}

func sum(x []int) int {
	if len(x) == 0 {
		return 0
	}

	return x[0] + sum(x[1:])
}
