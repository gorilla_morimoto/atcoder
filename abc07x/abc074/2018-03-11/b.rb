n, k, = 2.times.map {gets.to_i}
x = gets.split.map(&:to_i)

y = x.map do |v|
  if v <= (k - v).abs
    v * 2
  else
    (k - v).abs * 2
  end
end

puts y.inject(:+)
