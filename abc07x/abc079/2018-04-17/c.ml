let c2i c = Char.code c - Char.code '0'

let print s ops =
  Printf.printf "%c%s%c%s%c%s%c=7\n" s.[0] ops.(0) s.[1] ops.(1) s.[2] ops.(2) s.[3] ;
  ignore (raise_notrace Exit)

let solve s =
  let rec f i sum ops =
    if i >= 4 then
      let ar = List.rev ops |> Array.of_list in
      (if sum = 7 then print s ar else ())
    else
      let n = c2i s.[i] in
      f (i + 1) (sum + n) ("+" :: ops) ;
      f (i + 1) (sum - n) ("-" :: ops) ;
  in
  f 1 (c2i s.[0]) []

let () = solve (read_line ())
