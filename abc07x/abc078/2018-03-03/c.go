package main

import (
	"fmt"
	"math"
)

func main() {
	var n, m float64
	fmt.Scan(&n, &m)
	// 一回の実行時間 * すべて成功するのに平均で何回の試行を要するか
	// （Mが初めて全部成功するまでの試行回数の期待値）
	fmt.Println(((n-m)*100 + m*1900) * math.Pow(2, m))
}
