# ABC078

2018-04-17

## テストケース

https://www.dropbox.com/sh/arnpe0ef5wds8cv/AAAmb4qj_teNU532sbbHhsdYa/ARC085?dl=0

## A - HEX

- asciiにして比較すればよさそう

## B - ISU

- (x - z) / (y + z)で一撃っぽい

## C - HSI

> 1回の試行で起きる確率がpの事象Aについて，Aが起きるまで試行を繰り返し行うとすると，その回数の期待値 E は 1/p

https://detail.chiebukuro.yahoo.co.jp/qa/question_detail/q1243791537

- ということなので、
  - Mケースが完了するまでにかかる時間の期待値は(1900*m) * (2^m) // 2^mは期待値
  - 残りにかかる時間は(100*n-m) * (2^m)
  - これらの和
