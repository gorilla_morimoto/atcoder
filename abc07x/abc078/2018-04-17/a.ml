let f x y =
  match Char.code x - Char.code y with
  | 0 -> "="
  | z when z < 0 -> "<"
  | _ -> ">"

let () = Scanf.scanf "%c %c" f |> print_endline