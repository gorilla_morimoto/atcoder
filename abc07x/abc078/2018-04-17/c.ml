let pow x n =
  let rec aux i tmp = if i = 1 then tmp else aux (i - 1) (tmp * x) in
  aux n x

let f n m =  (1900 * m) * (pow 2 m) + (100 * (n - m)) * (pow 2 m)

let () = Scanf.scanf "%d %d" f |> Printf.printf "%d\n"