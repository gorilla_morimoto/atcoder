let q2a s = String.map (function '?' -> 'a' | c -> c) s

let f s' t =
  let lens = String.length s' in
  let lent = String.length t in
  let rec inner si ti =
    if ti >= lent then true
    else
      match (s'.[si], t.[ti]) with
      | a, b when a = b -> inner (si + 1) (ti + 1)
      | '?', _ -> inner (si + 1) (ti + 1)
      | _, _ -> false
  in
  let rec outer si =
    if si > lens - lent then "UNRESTORABLE"
    else if inner si 0
    then String.sub s' 0 si ^ t ^ String.sub s' (si + lent) (lens - si - lent)
    else outer (si + 1)
  in
  outer 0


let () = Scanf.scanf "%s\n%s" f |> q2a |> print_endline
