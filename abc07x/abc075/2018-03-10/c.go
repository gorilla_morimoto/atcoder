package main

import "fmt"

var (
	n, m    int
	graph   [50][50]bool
	visited [50]bool
)

func main() {
	fmt.Scan(&n, &m)
	a := make([]int, m)
	b := make([]int, m)
	for i := 0; i < m; i++ {
		fmt.Scanln(&a[i], &b[i])

		a[i]--
		b[i]--

		graph[a[i]][b[i]] = true
		graph[b[i]][a[i]] = true
	}

	ans := 0

	for i := 0; i < m; i++ {
		graph[a[i]][b[i]] = false
		graph[b[i]][a[i]] = false
		for j := 0; j < n; j++ {
			visited[j] = false
		}
		dfs(0)

		bridge := false

		for j := 0; j < n; j++ {
			if visited[j] == false {
				bridge = true
			}
		}

		if bridge {
			ans++
		}

		graph[a[i]][b[i]] = true
		graph[b[i]][a[i]] = true
	}

	fmt.Println(ans)

}

func dfs(v int) {
	visited[v] = true

	for v2 := 0; v2 < n; v2++ {
		if graph[v][v2] == false {
			continue
		}
		if visited[v2] == true {
			continue
		}
		dfs(v2)
	}
}
