# https://beta.atcoder.jp/contests/abc075/submissions/2356684
def dfs(v)
  @used[v] = true
  G[v].each do |u|
    next if @used[u]
    next if @ex == [v, u]
    dfs(u)
  end
end

N, M = gets.split.map(&:to_i)
G = Array.new(N) { [] }
es = []
M.times do
  a, b = gets.split.map(&:to_i)
  G[a - 1] << b - 1
  G[b - 1] << a - 1
  es << [a - 1, b - 1]
end

ans = 0
es.each do |e|
  @ex = e
  @used = Array.new(N, false)
  dfs(e[0])
  ans += 1 unless @used[e[1]]
end

puts ans
