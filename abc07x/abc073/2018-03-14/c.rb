h = {}

gets.to_i.times do
  a = gets.to_i
  h[a] = h.key?(a) ? !h[a] : true
end

puts h.values.select { |v| v }.size
