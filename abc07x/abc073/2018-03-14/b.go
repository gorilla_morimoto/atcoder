package main

import "fmt"

var (
	n    int
	l, r int
)

func main() {
	fmt.Scan(&n)

	count := 0
	for i := 0; i < n; i++ {
		fmt.Scanln(&l, &r)
		count += r - l + 1
	}

	fmt.Println(count)
}
