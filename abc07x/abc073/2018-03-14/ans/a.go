package main

import "fmt"

var (
	n int
)

func main() {
	fmt.Scan(&n)
	if n%10 == 9 || n/10 == 9 {
		fmt.Println("Yes")
	} else {
		fmt.Println("No")
	}
}
