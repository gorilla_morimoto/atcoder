gets
count = 0

while line = gets
  l, r = line.split.map(&:to_i)
  count += r - l + 1
end

puts count
