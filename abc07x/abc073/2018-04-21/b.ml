let f n =
  let rec g n' sum =
    if n' = 0 then sum
    else
      let v = Scanf.scanf "%d %d\n" (fun l r -> r - l + 1) in
      g (n' - 1) (sum + v)
  in
  g n 0


let () = Scanf.scanf "%d\n" f |> Printf.printf "%d\n"
