s = gets.chomp
count = 0

while s =~ /(25)+/
  s.gsub!(/(25)+/, '')
  count += 1
end

puts s.empty? ? count : -1
