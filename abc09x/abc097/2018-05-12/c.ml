module Ss = Set.Make (String)

let s = read_line ()

let len = String.length s

let k = read_int ()

let rec inner i j set =
  if j > len - i || j > k then set
  else
    let sub = String.sub s i j in
    let set' = Ss.add sub set in
    inner i (succ j) set'


let rec outer i set =
  if i <= len - 1 then
    let set' = inner i 1 set in
    outer (succ i) set'
  else set


let () =
  let ar =
    outer 0 Ss.empty |> Ss.elements |> List.fast_sort String.compare
    |> Array.of_list
  in
  print_endline ar.(k - 1)

