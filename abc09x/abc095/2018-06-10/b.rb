# frozen_string_literal: true

n, x, *m = $stdin.read.split.map(&:to_i)
ans = n
x -= m.inject(&:+)
p ans + (x / m.min)
