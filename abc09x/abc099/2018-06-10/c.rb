# frozen_string_literal: true

a = [1, 6, 9]
b = [6, 9]

2.upto(6) do |i|
  b.each do |x|
    a.push x**i
  end
end

a.reverse!
n = gets.to_i

rest = n
ans = 0

a.each do |i|
  next unless (rest % i).zero?
  break if i == 1
  ans += (rest / i)
  break
end

unless ans.zero?
  p ans
  exit
end

a.each do |i|
  printf("%d, %d, %d\n", i, rest, ans)
  if i <= rest
    ans += rest / i
    rest = rest % i
  end
  break if rest.zero?
end

p ans
