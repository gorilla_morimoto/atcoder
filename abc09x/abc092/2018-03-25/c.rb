n = gets.to_i
a = gets.split.map(&:to_i)

total = a[0].abs

0.upto(n - 2) do |i|
  total += (a[i + 1] - a[i]).abs
end

total += a[-1].abs

a.unshift(0)
1.upto(n) do |i|
  if i == n
    cost = total - (a[i] - a[i - 1]).abs - (0 - a[i]).abs + (0 - a[i - 1]).abs
  else
    cost = total - (a[i] - a[i - 1]).abs - (a[i + 1] - a[i]).abs + (a[i + 1] - a[i - 1]).abs
  end
  puts cost
end
