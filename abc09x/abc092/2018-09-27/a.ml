let () =
  let f () = Scanf.scanf "%d\n%d\n" (fun a b -> min a b) in
  Printf.printf "%d\n" (f () + f ())