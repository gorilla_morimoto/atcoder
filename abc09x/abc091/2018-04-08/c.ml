let scan_line () = Scanf.scanf "%d %d\n" (fun a b -> (a, b))

let n = Scanf.scanf "%d\n" (fun x -> x)

let init n =
  let rec aux i ls =
    if i = 0 then List.fast_sort (fun i j -> fst i - fst j) ls
    else aux (i - 1) (scan_line () :: ls)
  in
  aux n []


let r' = init n
let b' = init n

let rec solve r count = function
  | [] -> count
  | bp :: l ->
    match List.filter (fun rp -> fst rp < fst bp && snd rp < snd bp) r with
    | [] -> solve r count l
    | r' ->
      let v = List.fast_sort (fun i j -> snd j - snd i) r' |> List.hd in
      solve (List.remove_assoc (fst v) r) (count + 1) l


let () = solve r' 0 b' |> Printf.printf "%d\n"
