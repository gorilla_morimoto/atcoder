n = gets.to_i
s = Hash.new { 0 }
n.times do
  s[gets.chomp] += 1
end

s.sort { |(_k1, v1), (_k2, v2)| v2 <=> v1 }

t = Hash.new { 0 }
m = gets.to_i
m.times do
  t[gets.chomp] += 1
end

tmp = 0

s.each do |k, _v|
  next if (s[k] - t[k]) < 0
  tmp = (s[k] - t[k]) if tmp < (s[k] - t[k])
end

puts tmp
