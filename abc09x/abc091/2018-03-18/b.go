package main

import "fmt"

var (
	n, m int
	tmp  string
	ss   = make(map[string]int)
	ts   = make(map[string]int)
)

func main() {
	fmt.Scan(&n)
	for i := 0; i < n; i++ {
		fmt.Scan(&tmp)
		ss[tmp]++
	}

	fmt.Scan(&m)
	for i := 0; i < m; i++ {
		fmt.Scan(&tmp)
		ts[tmp]++
	}

	score := 0
	for k := range ss {
		if ss[k]-ts[k] > score {
			score = ss[k] - ts[k]
		}
	}

	fmt.Println(score)
}
