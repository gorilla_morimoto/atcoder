# frozen_string_literal: true

h, w = gets.split.map(&:to_i)

arr = []
h.times do
  arr.push gets.chomp
end

dim = [[0, 1], [1, 0], [0, -1], [-1, 0]]
arr.each_with_index do |row, i|
  row.chars.each_with_index do |c, j|
    next if c == '.'
    exist_sharp = false

    dim.each do |xy|
      next if xy[0] + j >= w || xy[1] + i >= h
      exist_sharp = true if arr[i + xy[1]][j + xy[0]] == '#'
      break if exist_sharp
    end

    unless exist_sharp
      puts 'No'
      exit
    end
  end
end

puts 'Yes'
