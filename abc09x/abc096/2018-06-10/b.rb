# frozen_string_literal: true

*arr, k = $stdin.read.split.map(&:to_i)
ma = arr.max
p(arr.inject(:+) + (ma * (2**k) - ma))
