package main

import "fmt"

var (
	l1, l2, l3 string
)

func main() {
	fmt.Scan(&l1, &l2, &l3)
	fmt.Printf("%c%c%c\n", l1[0], l2[1], l3[2])
}
