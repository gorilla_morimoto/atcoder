let a = read_line ()
let b = read_line ()
let c = read_line ()

let f s =
  let len = String.length s in
  if len = 0 then None
  else Some (s.[0], String.sub s 1 (len - 1))

let rec main who a' b' c' =
  match who with
  | 'a' ->
    (match f a' with | None -> "A" | Some (x, s) -> main x s b' c')
  | 'b' -> 
    (match f b' with | None -> "B" | Some (x, s) -> main x a' s c')
  | _ ->  
    (match f c' with | None -> "C" | Some (x, s) -> main x a' b' s)


let () = print_endline (main 'a' a b c)