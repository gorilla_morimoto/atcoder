let n = Scanf.scanf "%d\n" (fun x -> x)

let rec solve i sumt suma =
  if i = 0 then (sumt + suma)
  else
    let t, a = Scanf.scanf "%d %d\n" (fun x y -> x, y) in
    let x = 
      if (suma / a) < (sumt / t) then
        (if sumt mod t = 0 then sumt / t else (sumt / t) + 1)
      else
        (if suma mod a = 0 then suma / a else (suma / a) + 1)
    in 
    solve (i - 1) (t * x) (a * x)

let () = solve n 1 1 |> Printf.printf "%d\n"