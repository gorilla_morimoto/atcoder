let solve s =
  let len = String.length s in
  let rec aux i pre count =
    if i = len then count
    else if pre <> s.[i] then aux (i + 1) s.[i] (count + 1)
    else aux (i + 1) pre count
  in
  aux 0 s.[0] 1

let () = solve (read_line ()) - 1 |> Printf.printf "%d\n"
