let scan_line () = Scanf.scanf "%d %d %d\n" (fun a b c -> (a, b, c))

let w, h, n = scan_line ()

let rec f i l r d u =
  if i = 0 then max (r - l) 0 * max (u - d) 0
  else
    let x, y, a = scan_line () in
    match a with
    | 1 -> f (i - 1) (max l x) r d u
    | 2 -> f (i - 1) l (min r x) d u
    | 3 -> f (i - 1) l r (max d y) u
    | _ -> f (i - 1) l r d (min u y)


let () = Printf.printf "%d\n" (f n 0 w 0 h)
