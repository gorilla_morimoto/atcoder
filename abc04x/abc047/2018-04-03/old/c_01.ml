(*最初に自分で考えたコード TLEした*)

(*無駄に複雑なことをしていた*)
let one_color s =
  let len = String.length s in
  let black = String.make len 'B' in
  if s = black then true
  else
    let white = String.make len 'W' in
    if s = white then true else false

(*両サイドから見ていくのが必要だと思っていた*)
let flip_l s =
  let len = String.length s in
  let newcolor = if s.[0] = 'W' then 'B' else 'W' in
  (*文字列を頭から見ていって新しい色が出てくるまでカウント*)
  let rec aux i =
    if i = len then String.make len newcolor
    else if s.[i] <> newcolor then aux (i+1)
    (*新しくひとつ置くのでi+1*)
    else (String.make (i+1) newcolor) ^ (String.sub s i (len - i))
  in 
  aux 0

let flip_r s =
  let len = String.length s in
  let newcolor = if s.[(len-1)] = 'W' then 'B' else 'W' in
  (*文字列を後ろから見ていって新しい色が出てくるまでカウント*)
  let rec aux i =
    if i = -1 then String.make len newcolor
    else if s.[i] <> newcolor then aux (i-1)
    (*新しくひとつ置くのでi+1*)
    else (String.sub s 0 (i+1)) ^ (String.make (len - i) newcolor)
  in 
  aux (len - 1)

let rec solve s count lr =
  if one_color s then count
  else if lr = 'l' then solve (flip_l s) (count + 1) lr
  else solve (flip_r s) (count + 1) lr

let () =
  let s = Scanf.scanf "%s" (fun s -> s) in
  Printf.printf "%d\n" (solve s 0 'l')
