(* リストの合計値を2で割ったものがリストの中に存在したらYes *)
let () =
  let ls = Scanf.scanf "%d %d %d" (fun a b c -> [a; b; c]) in
  (if List.mem ((List.fold_left (+) 0 ls) / 2) ls then "Yes" else "No")
  |> Printf.printf "%s\n"
