let id x = x

let n = Scanf.scanf "%d\n" id

let ar = Array.init n (fun _ -> Scanf.scanf "%d " id)

let avg_rem =
  let sum = Array.fold_left ( + ) 0 ar in (sum / n, sum mod n)

let calc avg = 
  Array.fold_left (fun pre x -> pre + (x - avg) * (x - avg)) 0 ar

let () =
  (match avg_rem with
   | (avg, 0) -> calc avg
   | (avg, _) -> min (calc avg) (calc (avg + 1)))
  |> Printf.printf "%d\n"

