# 某氏が考えたアルゴリズム
# WA、TLEです
def unbalance?(s)
  s.chars.any? { |c| s.count(c) >= (s.length / 2.0) }
end

s = gets.chomp
h = Hash.new { 0 }

s.chars do |c|
  h[c] += 1
end
h.each do |k, v|
  next if v < 2
  rear = s.rindex(k)
  front = s.index(k)
  len = rear - front + 1
  tmp = s[s.index(k), len]
  if unbalance?(tmp)
    printf("%d %d\n", front+1, rear+1)
    exit
  end
end

printf("-1 -1\n")
