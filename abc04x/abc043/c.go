package main

import "fmt"

func main() {
	var n int
	fmt.Scanln(&n)

	as := make([]int, n)
	sum := 0

	for i := 0; i < n; i++ {
		fmt.Scan(&as[i])
		sum += as[i]
	}

	avg := sum / n
	rem := sum % n
	ans := 0

	if rem == 0 {
		for _, v := range as {
			ans += (v - avg) * (v - avg)
		}
	} else {
		off := 0
		up := 0

		for _, v := range as {
			off += (v - avg) * (v - avg)
			up += (v - (avg + 1)) * (v - (avg + 1))
		}

		if up < off {
			ans = up
		} else {
			ans = off
		}
	}

	fmt.Println(ans)
}
