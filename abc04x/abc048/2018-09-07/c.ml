let n, x = Scanf.scanf "%d %d\n" (fun n x -> (n, x))

let ar = Array.init n (fun _ -> Scanf.scanf "%d " (fun x -> x))

let ar' = Array.copy ar

let count = ref 0

let count' = ref 0

let () =
  (*奇数番目の数字を調整する -> こっちは必要なかった*)
  for i = 0 to n - 2 do
    let sum = ar.(i) + ar.(i + 1) in
    if sum > x then count := !count + (sum - x)
  done ;
  (*偶数番目の数字を調整する*)
  for i = 0 to n - 2 do
    let sum = ar'.(i) + ar'.(i + 1) in
    if sum > x then (
      count' := !count' + (sum - x) ;
      ar'.(i + 1) <- max 0 (ar'.(i + 1) - (sum - x)) )
  done ;
  Printf.printf "%d\n" (min !count !count')

