n, a = gets.split.map(&:to_i)
xs = gets.split.map(&:to_i)

count = 0
1.upto(n) do |i|
  xs.combination(i) do |ar|
    avg, rem = ar.inject(:+).divmod(i)
    count += 1 if avg == a && rem.zero?
  end
end

p count
