# https://beta.atcoder.jp/contests/abc044/submissions/1148337
s=gets.chomp
puts s.chars.all?{|c| s.count(c).even? } ? 'Yes' : 'No'
