let n, k = Scanf.scanf "%d %d\n" (fun a b -> a, b)
let ds = Array.init k (fun _ -> Scanf.scanf "%c " (fun x -> x))

let rec solve p =
  let s = string_of_int p in
  if (Array.fold_left (fun pre c ->
      pre || String.contains s c) false ds)
  then solve (p + 1)
  else p

let () = solve n |> Printf.printf "%d\n"