let () =
  Scanf.scanf "%d %d %d" (fun a b c ->
      match (List.sort compare [a;b;c]) with
      | [5; 5; 7] -> "YES"
      | _ -> "NO")
  |> print_endline