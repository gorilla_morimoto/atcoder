package main

import (
	"fmt"
	"strconv"
	"strings"
)

var (
	n, k int
	ds   []string
)

func main() {
	fmt.Scan(&n, &k)
	ds = make([]string, k)
	for i := 0; i < k; i++ {
		fmt.Scan(&ds[i])
	}

	fmt.Println(solve(n))
}

func solve(p int) int {
	s := strconv.Itoa(p)
	for _, v := range ds {
		if strings.Contains(s, v) {
			return solve(p + 1)
		}
	}

	return p
}
