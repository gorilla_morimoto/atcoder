let n, y = Scanf.scanf "%d %d" (fun a b -> (a, b))

let rec solve tt =
  let rec aux ft =
    if tt + ft > n then None
    else
      let t = n - tt - ft in
      if tt * 10000 + ft * 5000 + t * 1000 = y then Some (tt, ft, t) else aux (ft + 1)
  in
  if tt > n then (-1, -1, -1)
  else match aux 0 with Some x -> x | None -> solve (tt + 1)


let () =
  let tt, ft, t = solve 0 in Printf.printf "%d %d %d\n" tt ft t
