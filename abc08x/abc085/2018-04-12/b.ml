let mk_ls n =
  let rec aux i tmp =
    if i = 0 then List.sort_uniq compare tmp else aux (i - 1) (read_int () :: tmp)
  in
  aux n []


let rec solve ans = function
  | [] | [_] -> ans
  | h :: (n :: _ as t) ->
    if h = n then ans else solve (ans + 1) t

let () = read_int () |> mk_ls |> solve 1 |> Printf.printf "%d\n"