package main

import (
	"bufio"
	"os"
)

func main() {
	sc := bufio.NewScanner(os.Stdin)
	// 最初の値は長さなので必要ない
	sc.Scan()
	// 長さのカウンタ
	l := 0
	// 重複を排除するためのmap
	m := map[string]bool{}

	for sc.Scan() {
		// mapにアクセスすると値と「取得できたか」のboolが得られる
		if _, haveTheValue := m[sc.Text()]; !haveTheValue {
			l++
			m[sc.Text()] = true
		}
	}

	println(l)
}
