let f s =
  let len = String.length s in
  let rec aux i c =
    if i >= len then c
    else if s.[i] = '1' then aux (i + 1) (c + 1)
    else aux (i + 1) c
  in
  aux 0 0

let () = f @@ read_line () |> Printf.printf "%d\n"