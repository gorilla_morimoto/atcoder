package main

import (
	"bufio"
	"fmt"
	"os"
	"strconv"
	"strings"
)

func main() {
	sc := bufio.NewScanner(os.Stdin)

	sc.Scan()
	fline := strings.Split(sc.Text(), " ")
	k, _ := strconv.Atoi(fline[1])

	sc.Scan()
	a := strings.Split(sc.Text(), " ")

	m := map[string]int{}

	for _, v := range a {
		m[v]++
	}

	// マップの長さがKよりも小さくなるまで
	// 最も要素数が少ないキーをマップから削除していき
	// マップから削除したときに、該当キーの要素をdeletedに足していく

	deleted := 0
	for len(m) > k {
		// 最も要素数が少ない数の要素数
		tmp := 0
		// 最も要素数が少ない数のキー
		s := ""
		for i, v := range m {
			if tmp == 0 || v < tmp {
				s = i
				tmp = v
			}
		}
		deleted += m[s]
		delete(m, s)
	}
	fmt.Println(deleted)

}
