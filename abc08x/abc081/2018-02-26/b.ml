let n = read_int ()

let a =
  let rec f n ls = 
    if n = 0 then ls
    (*scanf の書式にスペースを入れなかったらエラーになる*)
    else f (n - 1) (Scanf.scanf "%d " (fun x -> x) :: ls)
  in 
  f n []

let div2 n =
  let rec aux n count =
    if n mod 2 = 1 then count else aux (n / 2) (count + 1) 
  in aux n 0


let () =
  List.fold_left (fun x y -> min x (div2 y)) max_int a
  |> Printf.printf "%d\n"