package main

import (
	"bufio"
	"fmt"
	"math"
	"os"
	"sort"
	"strconv"
	"strings"
)

func main() {
	sc := bufio.NewScanner(os.Stdin)
	// 最初の値はいらない
	sc.Scan()
	sc.Scan()
	in := strings.Split(sc.Text(), " ")
	a := make([]int, 0, len(in))
	for _, v := range in {
		n, _ := strconv.Atoi(v)
		a = append(a, n)
	}

	// すでにソートされていた場合
	if sort.IsSorted(sort.IntSlice(a)) {
		fmt.Println(0)
		os.Exit(0)
	}

	// すべての数が正（負）だった場合
	p := 0          // 正の数の個数
	m := 0          // 負の数の個数
	count := 0      // 操作の回数を保持
	x := [][2]int{} // 操作を行った場所を保持
	for _, v := range a {
		if v >= 0 {
			p++
		} else if v < 0 {
			m++
		}
	}
	// すべての数が正だった場合
	// aのインデックス0から自身の値を次の要素に足していく
	if p == len(a) {
		for i := 0; i < len(a); i++ {
			if i != 0 {
				count++
				x = append(x, [2]int{i - 1, i})
				a[i] += a[i-1]
			}
		}
		fmt.Println(count)
		for _, w := range x {
			// 設問では1-indexedなので1を足します
			fmt.Printf("%d %d\n", w[0]+1, w[1]+1)
		}

		// すべての数が負だった場合
		// aの最後の要素から前の値に自身の値を足していく
	} else if m == len(a) {
		for i := len(a) - 1; i > -1; i-- {
			if i != 0 {
				count++
				x = append(x, [2]int{i, i - 1})
				a[i-i] += a[i]
			}
		}
		fmt.Println(count)
		for _, w := range x {
			fmt.Printf("%d %d\n", w[0]+1, w[1]+1)
		}

		// 正負混じりの場合
	} else {
		// 最大値
		max := -1000000
		whereIsMax := -1
		// 最小値
		min := 1000000
		whereIsMin := -1
		for i, v := range a {
			// 初期化
			if i == 0 {
				max = v
				whereIsMax = i
				min = v
				whereIsMin = i
				continue
			}
			if v > max {
				max = v
				whereIsMax = i
			} else if v < min {
				min = v
				whereIsMin = i
			}
		}

		// 解説にあるとおり、|MAX| >= |MIN|を判定して
		// すべてを正（負）にする

		if math.Abs(float64(max)) >= math.Abs(float64(min)) {
			for i := 0; i < len(a); i++ {
				// すべてを正の数にする
				if i != whereIsMax {
					count++
					x = append(x, [2]int{whereIsMax, i})
					a[i] += a[whereIsMax]
				}
			}
			// 上のp == len(a)と同じ処理
			for i := 0; i < len(a); i++ {
				if i != 0 {
					count++
					x = append(x, [2]int{i - 1, i})
					a[i] += a[i-1]
				}
			}
			fmt.Println(count)
			for _, w := range x {
				// 設問では1-indexedなので1を足します
				fmt.Printf("%d %d\n", w[0]+1, w[1]+1)
			}

		} else {
			for i := 0; i < len(a); i++ {
				// すべてを負の数にする
				if i != whereIsMin {
					count++
					x = append(x, [2]int{whereIsMin, i})
					a[i] += a[whereIsMin]
				}
			}
			// 上のm == len(a)と同じ処理
			for i := len(a) - 1; i > -1; i-- {
				if i != 0 {
					count++
					x = append(x, [2]int{i, i - 1})
					a[i-i] += a[i]
				}
			}
			fmt.Println(count)
			for _, w := range x {
				fmt.Printf("%d %d\n", w[0]+1, w[1]+1)
			}
		}

	}
}
