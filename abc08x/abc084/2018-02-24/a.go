package main

import (
	"bufio"
	"os"
	"strconv"
)

func main() {
	sc := bufio.NewScanner(os.Stdin)
	sc.Scan()
	in, _ := strconv.Atoi(sc.Text())
	println(48 - in)
}
