package main

import (
	"bufio"
	"fmt"
	"os"
	"strconv"
	"strings"
)

func main() {
	sc := bufio.NewScanner(os.Stdin)

	var (
		time          int // 次の駅までの所要時間
		start         int // 始発の時刻
		interval      int // 始発後の電車の発車間隔
		toNextStation int // 次の駅までの最短時間
	)

	// 入力を受け取るためのスライス
	// 最初の行は入れないため、最大長は500-1
	lines := make([][3]int, 0, 499)

	sc.Scan()
	howManyStations, _ := strconv.Atoi(sc.Text())

	for sc.Scan() {
		line := strings.Split(sc.Text(), " ")

		time, _ = strconv.Atoi(line[0])
		start, _ = strconv.Atoi(line[1])
		interval, _ = strconv.Atoi(line[2])

		lines = append(lines, [3]int{time, start, interval})
	}

	// 2駅目から対象の駅を減らすためのループ変数iを定義
	// 1 -> 2 -> ... -> N
	// 2 -> 3 -> ... -> N としたい
	for i := 0; i < howManyStations-1; i++ {
		toNextStation = 0
		for j, v := range lines {
			if j < i {
				continue
			}

			if toNextStation > v[1] {
				for v[1] < toNextStation {
					v[1] += v[2]
				}
			}
			toNextStation = v[0] + v[1]
		}
		fmt.Println(toNextStation)
	}
	fmt.Println(0)
}
