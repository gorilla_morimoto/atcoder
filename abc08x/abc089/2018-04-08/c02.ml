(*main以下に実際に計算する関数を入れた*)
(*可読性が下がる気がする*)
open Hashtbl

let h = create 5

let ls = [|'M'; 'A'; 'R'; 'C'; 'H'|]

let rec inith n =
  if n > 0 then
    let c = (read_line ()).[0] in
    match c with
    | 'M' | 'A' | 'R' | 'C' | 'H' ->
        let v = try find h c with _ -> 0 in
        replace h c (v + 1) ;
        inith (n - 1)
    | _ -> inith (n - 1)


let rec main i tmp =
  let rec calc i j k tmp =
    if k > 4 then tmp
    else
      calc i j (k + 1)
        ( tmp
        + (try find h ls.(i) with _ -> 0)
        * (try find h ls.(j) with _ -> 0)
        * try find h ls.(k) with _ -> 0 )
  in
  let rec inner i j tmp =
    if j > 3 then tmp
    else
      let tmp' = calc i j (j + 1) tmp in
      inner i (j + 1) tmp'
  in
  if i > 2 then tmp
  else
    let tmp' = inner i (i + 1) tmp in
    main (i + 1) tmp'


let () =
  inith (read_int ()) ;
  main 0 0 |> Printf.printf "%d\n"

