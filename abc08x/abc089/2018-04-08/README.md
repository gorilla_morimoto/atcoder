# ABC089

2018-04-08

## テストケース

https://www.dropbox.com/sh/arnpe0ef5wds8cv/AADM5wTmoNNdE-JpFpW7Rt8qa/ABC089?dl=0

## A - Grouping 2

- 3で割ったときの商でよさそう

## B - Hina Arare

- 頭からひとつずつ見ていく
  - Yが出てきた時点でFour、最後まで見終わったらThree

## C - March

- それぞれの頭文字から始まる人をマップで数える

- 1人以上存在する頭文字から3つ取り出す

- マップの値をかけあわせる
