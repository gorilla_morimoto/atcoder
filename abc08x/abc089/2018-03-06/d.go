package main

import (
	"fmt"
)

var (
	h, w, d int
	a       = make(map[int][2]int)
	x       int
	q       int
	l, r    int
	lr      = make([][2]int, 0, q)
	mp      int
	cost    = make(map[int]int)
)

func main() {
	fmt.Scanln(&h, &w, &d)
	for i := 0; i < h; i++ {
		for j := 0; j < w; j++ {
			fmt.Scan(&x)
			a[x] = [2]int{i + 1, j + 1}
		}
	}

	fmt.Scan(&q)
	for i := 0; i < q; i++ {
		fmt.Scanln(&l, &r)
		lr = append(lr, [2]int{l, r})
	}

	cost[1] = 1
	for i := d + 1; i <= h*w; i++ {
		cost[i] = cost[i-d] +
			abs(a[i][0]-a[i-d][0]) + abs(a[i][1]-a[i-d][1])
	}

	for _, v := range lr {
		fmt.Println(
			cost[v[1]] - cost[v[0]],
		)
	}

}

func abs(x int) int {
	if x < 0 {
		return -x
	}
	return x
}
