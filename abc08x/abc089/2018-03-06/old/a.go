package main

import "fmt"

func main() {
	// 入力
	var n int
	fmt.Scan(&n)

	var count int
	for n > 2 {
		count++
		n -= 3
	}

	// 出力
	fmt.Println(count)

}
