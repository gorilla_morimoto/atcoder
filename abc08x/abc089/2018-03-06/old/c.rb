def nodup(a, b, c)
  !(a.chr == b.chr || b.chr == c.chr || c.chr == a.chr)
end

# getsで最初の行のNが入ってくるが、条件に合うものだけをpushとしているので問題ない
s = []
# Rubocop的にはよくない書き方らしい
while line = gets
  # chompしないと改行コードまで入ってくる
  # String.chrは最初の文字を取り出す（line[0]と同じっぽい）
  s.push(line.chomp) if 'MARCH'.split('').include?(line.chr)
end

count = 0

# x.upto(y)はx <= n <= y のnだけ繰り返す
0.upto(s.length - 3) do |i|
  (i + 1).upto(s.length - 2) do |j|
    (j + 1).upto(s.length - 1) do |k|
      count += 1 if nodup(s[i], s[j], s[k])
    end
  end
end

puts count
