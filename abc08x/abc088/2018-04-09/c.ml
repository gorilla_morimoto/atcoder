let ar =
  Array.init 3 (fun _ -> Scanf.scanf "%d %d %d\n" (fun a b c -> a, b, c))

let rec eq a b c = a = b && b = c && c = a

let rec solve () =
  let c11, c12, c13 = ar.(0)
  and c21, c22, c23 = ar.(1)
  and c31, c32, c33 = ar.(2) in
  eq (c11 - c21) (c12 - c22) (c13 - c23)
  && eq (c21 - c31) (c22 - c32) (c23 - c33)
  && eq (c11 - c12) (c21 - c22) (c31 - c32)
  && eq (c12 - c13) (c22 - c23) (c32 - c33)


let () = (if solve () then "Yes" else "No") |> print_endline
