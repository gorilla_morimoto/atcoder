package main

import (
	"fmt"
)

func main() {
	// nは金額、aは1円玉の枚数
	var (
		n, a int
	)
	fmt.Scan(&n, &a)

	// 剰余がa以下なら、ちょうど支払える
	if (n % 500) <= a {
		fmt.Println("Yes")
	} else {
		fmt.Println("No")
	}
}
