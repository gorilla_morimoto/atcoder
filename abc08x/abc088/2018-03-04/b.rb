# Nは必要ないので代入しません
gets.to_i
a = gets.split.map(&:to_i).sort.reverse

# ここで宣言をしておかないと、+=でエラーになった
alice = 0
bob = 0

a.each_with_index do |x, i|
  if (i % 2).zero?
    alice += x
  else
    bob += x
  end
end

p(alice - bob)
