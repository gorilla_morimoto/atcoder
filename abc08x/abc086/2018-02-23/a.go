package main

import (
	"bufio"
	"os"
	"strconv"
)

func getInputA() [2]int {
	var in [2]int

	sc := bufio.NewScanner(os.Stdin)
	sc.Split(bufio.ScanWords)

	sc.Scan()
	in[0], _ = strconv.Atoi(sc.Text())
	sc.Scan()
	in[1], _ = strconv.Atoi(sc.Text())

	return in
}

func main() {
	in := getInputA()
	if in[0]%2 == 0 || in[1]%2 == 0 {
		println("Even")
	} else {
		println("Odd")
	}
}
