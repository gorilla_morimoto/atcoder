package main

import (
	"bufio"
	"fmt"
	"math"
	"os"
	"strconv"
)

func getInputB() [2]int {
	var in [2]int

	sc := bufio.NewScanner(os.Stdin)
	sc.Split(bufio.ScanWords)

	sc.Scan()
	in[0], _ = strconv.Atoi(sc.Text())
	sc.Scan()
	in[1], _ = strconv.Atoi(sc.Text())

	return in
}

func main() {
	in := getInputB()
	s, _ := strconv.Atoi(fmt.Sprintf("%d%d", in[0], in[1]))
	// math.Sqrt()の結果に小数点以下の数値がついていたら0以上になる
	if math.Sqrt(float64(s))-math.Floor(math.Sqrt(float64(s))) > 0 {
		println("No")
	} else {
		println("Yes")
	}
}
