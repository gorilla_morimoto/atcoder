let n = Scanf.scanf "%d\n" (fun x -> x)

let ls =
  let rec aux n tmp =
    if n = 0 then List.rev tmp
    else
      let line = Scanf.scanf "%d %d %d\n" (fun t x y -> (t, x, y)) in
      aux (n - 1) (line :: tmp)
  in
  aux n [(0, 0, 0)]


let rec solve = function
  | [] | [_] -> "Yes"
  | (pt, px, py) :: ((nt, nx, ny) :: _ as t) ->
    let dift = nt - pt and difd = abs (nx - px) + abs (ny - py) in
    if difd <= dift && (dift + difd) mod 2 = 0 then solve t
    else "No"


let () = solve ls |> print_endline
