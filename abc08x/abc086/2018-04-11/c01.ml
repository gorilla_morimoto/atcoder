(*ハッシュテーブルを使う解法ですがスタックオーバーフローします*)

let n = Scanf.scanf "%d\n" (fun x -> x)

let h = Hashtbl.create n

let () = Hashtbl.add h 0 [(0, 0)]

let rec add t =
  if Hashtbl.mem h t then
    let ls = Hashtbl.find h t in
    List.map
      (fun pos ->
         let x, y = pos in
         [(x + 1, y); (x - 1, y); (x, y + 1); (x, y - 1)])
      ls
    |> List.concat |> Hashtbl.add h t
  else add (t - 1) ;
  add t


let rec can_reach t pos =
  if Hashtbl.mem h t then
    let ls = Hashtbl.find h t in
    List.mem pos ls
  else
    let () = add t in
    can_reach t pos


let rec solve i =
  if i = 0 then "Yes"
  else if Scanf.scanf "%d %d %d\n" (fun t x y -> can_reach t (x, y)) then
    solve (i - 1)
  else "No"


let () = solve n |> print_endline
