let sum_of_digits x =
  let rec f ans n =
    if n = 0 then ans else f (ans + (n mod 10)) (n / 10)
  in f 0 x

let () =
  Scanf.scanf "%d" (fun n -> 
      if n mod (sum_of_digits n) = 0 then "Yes" else "No")
  |> Printf.printf "%s\n"