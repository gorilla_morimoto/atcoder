package main

import (
	"bufio"
	"fmt"
	"os"
	"strconv"
	"strings"
)

func main() {
	sc := bufio.NewScanner(os.Stdin)
	sc.Scan()
	x, _ := strconv.Atoi(sc.Text())

	if x%f(x) == 0 {
		fmt.Println("Yes")
	} else {
		fmt.Println("No")
	}
}

func f(x int) int {
	in := strings.Split(strconv.Itoa(x), "")
	var sum int

	for _, v := range in {
		n, _ := strconv.Atoi(v)
		sum += n
	}

	return sum
}
