(*時間がかかる、バグがあります*)
let id x = x

let mk_ls n =
  let rec f i tmp =
    if i = 0 then List.fast_sort compare tmp
    else f (i - 1) (Scanf.scanf "%d " id :: tmp)
  in
  f n []


let solve ls =
  let rec aux c ans pre = function
    | [] -> ans + min c (abs (pre - c))
    | h :: t ->
      if h = pre then aux (c + 1) ans pre t
      else aux 1 (ans + min c (abs (pre - c))) h t
  in
  aux 1 0 (List.hd ls) (List.tl ls)


let () = mk_ls (Scanf.scanf "%d\n" id) |> solve |> Printf.printf "%d\n"
