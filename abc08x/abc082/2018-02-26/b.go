package main

import (
	"bufio"
	"fmt"
	"os"
	"sort"
	"strings"
)

// sは辞書順、tは辞書の逆順にソートして、
// 最初の文字を比較することで判定するというアルゴリズムです
func main() {
	sc := bufio.NewScanner(os.Stdin)

	sc.Scan()
	s := strings.Split(sc.Text(), "")
	sort.Strings(s)
	sc.Scan()
	t := strings.Split(sc.Text(), "")
	sort.Sort(sort.Reverse(sort.StringSlice(t)))

	if indexof(s[0]) < indexof(t[0]) {
		fmt.Println("Yes")
	} else {
		fmt.Println("No")
	}

}

func indexof(char string) int {
	alphabet := [26]string{"a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k", "l", "m", "n", "o", "p", "q", "r", "s", "t", "u", "v", "w", "x", "y", "z"}

	for i, v := range alphabet {
		if v == char {
			return i
		}
	}

	return -1
}
